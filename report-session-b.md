
# Remote wipe user testing session B

The same participants joined as in the social backup user testing session B. To recap:

- 5 Participants joined the session.
- The session was given in English.  All 5 were competent English speakers but none had English as their first language. Participant's native languages were German, Spanish and Arabic. Care was taken to be aware of language issues affecting comprehension of the tasks and usability issues.
- All 5 regularly used Android on their personal devices.
- All 5 had experience using messaging apps popular in western countries (One or more of Telegram, Signal, Whatsapp)
- 2 participants had worked in 'technical' jobs in the last 5 years
- 2 participants had an academic background - one had a Batchelor of arts, and one a Master of science 
- None used encrypted email regularly - an indicator of familiarity with concepts involving cryptographic keys
- None were familiar with Shamir's secret sharing. One was familiar with Facebook's social recovery feature but had not used it.

## Terminology

The terms 'wipee' and 'wiper' are used in this report for brevity. As these terms are ambiguous and confusing, they were never used in the session, and the roles were more explicitly described as 'the person having their account wiped' and 'one of the trusted contacts helping to activate the wipe'.

## Ice-breaker activity

After a 'trigger warning', and consent to discussing a potentially traumatic theme, participants were asked to get into the role of a political activist organising and attending a demonstration. They described their possible concerns and fears and what measures they might take to stay safe.

## UI Issues

![Remote wipe explainer screenshot](./img/Screenshot-explainer.png)

- Most participants did not understand that two remote wipe signals are required to activate the wipe. This was not made clear enough in the UI, both from the perspective of someone setting up the wipe and receiving the wipe.

- Regarding the 24 hour expiration time of the wipe signals, 4 participants thought this was appropriate, one thought it should be shorter, and suggested 3 hours.

## Proposed feature improvements

[Full list of possible improvements from both sessions](./possible-improvements.md)

### Revoke wiper status

Two participants expressed desire to be able to change which contacts are wipers.

### Use the same set of trusted contacts for social backup and remote wipe

Generally it was agreed that the social backup custodians and the wipers could be one and the same set of contacts. One participant described a situation where they might have a lot of trust in someone but not want them to be able to activate a remote wipe.  They gave the example of an older relative with little technical experience, who might accidentally activate a remote wipe.  They said this could be mitigated if other members of the support group were more technically competent, making it very unlikely that two wipers would activate a wipe accidentally.

## Bugs

- Similar to with social backup - there were issues when deleting contacts who had been added as wipers. Although it was possible to delete these contacts completely using the 'delete contact' option, the 'delete all messages' option failed to delete the wipe setup notification messages.
