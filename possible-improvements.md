
## Remote wipe feature - possible improvements following user testing

### Back end

- [x] Revoke wiper status
- Change wipe message expiration time - decided against this

### UI

- [x] Make it possible to change the set of trusted contacts
- [x] Add explainer screen during setup
- [x] Improve explainer screen during wipe activation
- [ ] Combine with social backup, having one trusted support group for both features

## Possible improvements for remote wipe following second test sessions

## Make it clear who the trusted wipers are

When the 'Remote Wipe' settings option is chosen after a remote wipe is already set up, a list of trusted contacts are displayed. From this screen it is possible to add or remove trusted contacts as well as disable the feature altogether.

![Screenshot display wipers](./img/screenshot-remote-wipe-display-wipers.png)
![Screenshot change wipers](./img/screenshot-remote-wipe-change-wipers.png)
![Screenshot remote wipe disabled](./img/screenshot-remote-wipe-disabled.png)

- [x] Make clear who the wipers are, with avatars
- [x] Add option to disable the remote wipe feature completely

### Setup success screen should be pop-up dialog, not full screen. 
![Screenshot remote wipe setup success new](./img/screenshot-remote-wipe-setup-success-new.png)

- [x] Replace full screen success message with pop-up dialog

### Wipe signal sent success screen should be pop-up dialog, not full screen.

![Screenshot remote wipe activated success new](./img/screenshot-remote-wipe-activated-success-new.png)

- [x] Replace full screen success message with pop-up dialog

### Confirmation for wipers on wipe being activated

Make it clear to the trusted contacts whether they have successfully activated a wipe, but sending them a confirmation message just before the data is wiped.

![Screenshot remote wipe confirmed](./img/screenshot-remote-wipe-confirmed.png)

- [x] Back-end - add 'wiped' message which is sent from the wiped device to the wipers just before activating the wipe
- [x] Front-end - notification on receiving a 'wiped' message for wipers.

- [ ] Add consent for being a wiper
