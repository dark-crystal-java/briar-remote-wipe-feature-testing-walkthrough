
# Explanation of project for user testing session

## What is dark crystal?

- a set of protocols, libraries, techniques and guidelines for secure management of sensitive data such as cryptographic keys.

- The idea is to make key management easy by emphasising trust between peers rather than individual responsibility.

- It is a toolkit for developers who want to bring secure and easy to use key management techniques to their project. It is particularly well suited for decentralised systems where authentication relies on keys stored on a peer’s device.

- Traditional client-server model - didn't have these key management problems - but at what cost?

- As a test case for our libraries / protocol we are building two features for Briar, a secure messaging app.

- Named dark crystal after the 1982 Jim Henson animated film involving saving the world by putting crystal shards back together

## Background of project 

Originally funded by Open Technology Fund, currently by National Democratic Institute. Previously received funding from Prototype Fund and Ethereum foundation.

Open tech fund 
- 'committed to advancing global internet freedom', 'supports projects counteracting repressive censorship and surveillance'

- Non-profit and independent - although funded indirectly by United States agency for Global Media

- Support many open source project including Tor and Wireguard VPN

NDI 
- 'a nonprofit, nonpartisan organization working to support and strengthen democratic institutions worldwide through citizen participation, openness and accountability in government'

- Legal entities in many countries.

- Support a wide range of projects.

- Currently running 'tech summer series' each Wednesday

## Briar

Secure messaging.  Works over Tor and other transports. No servers. Offline-first. The interface is similar to popular messaging apps, but works in a very different way. Encrypted database, must sign in with a password.

Aimed at high risk users with security concerns.

## Ice breaker activity

Imagine you are going away for a while and no-one will be at your home. Who would you give your spare key to?

## Social backup

Think about what happens when you loose a device, or it breaks, with the messaging systems you currently use.

Briar is self-authenticated using cryptographic keys.  There is no 'send me a new password' button.  There is no option to 'send me an SMS to verify it is me on a new device'.  Device or password loss means you are locked-out. Your identity is defined by keys on your own device, there is no database of all users somewhere.

We think self-authentication is massively empowering for users and want to support projects like Briar to do this without compromising usability.

Circle of trust idea. You assign a 'support group' of trusted contacts who collectively take the authoritative role of agreeing that 'this is me' when you want to recover your account.

Currently the recovery must take place in-person as this is the most secure.  Remote recovery is planned.

## Remote Wipe

Imagine you are an activist and using Briar to communicate with members of a political group. Then you are arrested at a demonstration, and your phone, which is signed into Briar, is taken. 

Using the same idea of a support group, you assign people to be able to wipe your Briar account, removing all contacts and messages.

