
# Remote wipe feature walk-through

Using this feature requires at least 3 Briar users, who take particular roles in the process: 
- One person is the 'wipee' - who can have their account wiped remotely
- All the others are all 'wipers' - trusted contacts who may remotely activate an account wipe.

> These names are a bit confusing - suggestions welcome!

**Reminder:**  The aim is to test the remote wipe feature - not to get general feedback about usability of briar

### Install package on at least 3 Android devices

Install the APK of the remote wipe debug build:

![QR code](https://gitlab.com/dark-crystal-java/briar-social-backup-feature-testing-walkthrough/-/raw/master/img/qr-apk.png)

https://darkcrystal.pw/assets/apk/briar-android-dark-crystal-debug.apk

Your browser should ask if you want to download the file and when opening it you may have to allow permissions to agree to installing apps from that source.

If you get a message like 'File cannot be opened' without asking about permissions, go to the device settings, search for 'Unknown' and there should be an option called something like 'Install apps from unknown sources'.

Also, with some android versions, Chrome will not let you open the APK, but if you find the downloaded file using the stock file manager app, you can open it from there.

### Set up briar

- Start Briar
- Choose 'Create new account'
- Choose a name and password

### The wipee adds all wipers as briar contacts:

On the wipee's device, as well as with each of the others:

- On the contacts screen, choose 'add' (the plus sign on bottom right), followed by 'Add nearby'
- Agree to permissions questions
- Scan each other's QR codes.
- Wait for contact to be added.

It will help if both devices are connected to the same wifi network, as otherwise contacts are added using bluetooth which with some devices does not work so well.  If it still doesn't work you can also 'add contact from a distance', which involves copying keys and sending them by some other means, eg: SMS.

### The wipee sets up the remote wipe feature:

Once the wipee has at least 2 contacts added, they can do the following:

- Open menu (hamburger icon at top left of contacts screen)
- Choose 'Settings' and then 'Remote Wipe'
- Select the trusted contacts using the check boxes
- Confirm the selection using the tick icon in top right corner.

You should see a confirmation message, and the wiper should get a notification that they have been added as wipers.

### The wipee gets arrested or captured

The wipee's device is now assumed to be in the hands of an adversary.

### The wipers activate a remote wipe

Two of the wipers do the following:
- Choose the wipee from their contacts list, to open the conversation screen.
- Choose the menu icon, with three dots in the top right corner of the screen
- Choose 'Activate Remote Wipe'
- An explainer screen should be displayed. Confirm by choosing 'Activate Remote Wipe'.
- A confirmation message should be displayed.

![Explainer screen](img/Screenshot-explainer.png)

Once two contacts have done this, the wiper's device should be signed out of briar, their account deleted, and Briar should be removed from android's list of recently used apps.

If the wiper is not signed into Briar, or has no connectivity, the wipe will take place whenever connectivity is regained and the messages are received.

The wipe signal messages expire after 24 hours.  If the wiper did not receive them after 24 hours, no wipe will occur when connectivity is regained. However, the contacts may re-activate the wipe at any time.

## Possible questions for participants after the session

- Should the social backup trusted contacts and remote wipe trusted contacts be rolled-into-one?  That is, you choose one set of trusted contacts who are both backup-holders and able to activate a wipe.  Are there some situations where you would want to appoint someone to one of these features but not the other?

- Would it be desirable to be able to revoke a contact's ability to activate a remote wipe?

- Is 24 hours a good length of expiration time for wipe messages?

- Two contacts are needed to activate a remote wipe.  Is this a good amount?  Would it be better to be able to choose the threshold?

- As a person sending the wipe activation signal, would it be useful to get a message confirming that the wipe was carried out?  Would such a message pose a security problem?

- Should a message be shown in the UI when signing in following a remote wipe activation?  Currently, after logging in, the remote wipe messages are received (providing the device has connectivity and is not in flight-mode), and the account is immediately deleted.  What if the user was coerced into entering their password? Could this be a good place to show a message explaining that the wipe was activated remotely and not by the user themselves? 
