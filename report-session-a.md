# User Testing Workshop 2021-06-23 Narrm

- 5 participants joined the session
    - 3 x android and 2 x iphone users
    - 1 person uses a password manager
    - none had heard of facebook social recovery
    - all participants use at least one secure messaging application (signal, telegram, other)
    - none of the participants had used PGP, 1 participant had heard of it, but didn't know what it was other than "something to do with encryption"
    - all participants considered themselves digital natives (they grew up with devices and 4/5 of the participants had grown up with access to the internet)

### Vignettes from workshops 1 and 2

- One person
    - Has all passwords to all accounts saved as a note in a place which they have shared with their close ones. This means that in the event people need access to their computer and accounts then they would know how to make this happen. Not a complicated process. This person has had experience with detainment and issues with borders so their risk strategy is to do with loved ones being able to assist them in such a scenario.

- Most
    - Most participants did not have a succession or disaster plan scenario in place. In the event something happened to them then they assumed that no-one would be able to access their devices or accounts

- One person
    - Had set up their iphone with ICE (in case of emergency) contacts. They had also set up 'Medical ID' (https://support.apple.com/en-au/HT207021) listing health issues and health care providers in case of an emergency. They stated that none of their friends were aware of this feature they were using but that they assumed their health care providers and emergency health care workers would know where to look on their device to access this info.

- One person
    - During the Trump reign, pre-pandemic, one participant had to travel to USA for work. They are from a region and have heritage from places which had led to people being detained on the USA border (or potentially being last minute blocked from entering). Prior to travelling they locked their social media accounts and also shared their password credentials to social media accounts. As someone with a large following, part of their risk mitigation strategy was for their 'Safety Team' to post on their behalf in the event they got detained. This strategy did not need to be used in the end. This person commented that interpersonal conflict resulted in them having to go through and change all passwords and revoke access to accounts due to a break down in relationship with someone within their 'Safety Team'. They commented that this was a stressful and emotionally difficult process to go through.

